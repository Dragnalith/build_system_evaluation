#pragma once

namespace drgn
{

int getVersion();
const char* getName();
const char* getPlatformName();
const char* getComputerServiceName();
}